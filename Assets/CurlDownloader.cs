﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CurlUnity;
[System.Serializable]
public class ModelOutput{
    public List<string> stderr;
    public ModelResult result;
    public List<string> stdout;
}

public class ModelResult{
    public List<List<float>> probabilities;
    public List<int> classes;
    public List<int> predictions; 
}

public class CurlDownloader
{
    CurlEasy curlDW = new CurlEasy();

    public CurlDownloader()
    {
        curlDW.SetHeader("Authorization"," Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vdWFkIiwicGFja2FnZU5hbWUiOiJIZWFsdGhjYXJlIiwicGFja2FnZVJvdXRlIjoiaGVhcnQiLCJpYXQiOjE1NjI1NzkzMDB9.nJOhhpNIGNhWBXSF2q8cSeahbzX_dq4Wpgv_5XJl4htH1m9mUwCqCspetA-9VFWIXeslkAG-_t9iWW4zJ9C2lyuXVQwTE4nr7qseFIg42HtYUKnkzRLmBTPV4QbKl6NXtAZqkRPa65CTBaLZZk7uCuXBlZJU3xdrLEa8S8is3Xzz6Y5Fbm_SRC9MGNlFV0GjF8do9Nsl1RMKXeeRV3QGZfxe6lU5kH94aPnLCB875W79ihdDYMBFOiyFuH1h_S5wGY4NJJpM5GD9ZHk8kb5aY9DRTZ0GtctVsokyejUZBlWKyR2lUSVlPKU-bQM273G7B8D2AOsH7W1P9g8gO26smQ");
        curlDW.SetHeader("Cache-Control","no-cache");
        curlDW.contentType = "application/json";
        curlDW.insecure = true;
        curlDW.uri = new System.Uri("https://169.51.49.149:31843/dmodel/v1/heart/pyscript/rf/score");
        curlDW.method = "POST";
        curlDW.forceHttp2 = true;
        curlDW.timeout = 5000;
        curlDW.performCallback = OnPerform;
    }

    public void CurlDownloadModel(HeartDiseaseModel model)
    {
        Debug.Log(Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace('"','\"'));
        curlDW.outText = "{\"args\":{\"input_json\":[" +Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace('"','\"') + "]}}";
        curlDW.MultiPerform(CurlMulti.DefaultInstance);
    }
    private void OnPerform(CURLE result, CurlEasy easy)
    {
        if (result == CURLE.OK)
    {
        //Debug.Log("Ya did it champ.");
        //Debug.Log(easy.inText);
        ModelOutput testoutput = Newtonsoft.Json.JsonConvert.DeserializeObject<ModelOutput>(easy.inText);
        Debug.Log("Prediction:" + testoutput.result.predictions[0]);
        LoginWrapper.wrapper.modelResponse = testoutput.result.predictions[0].ToString();
        LoginWrapper.wrapper.obtainedModelResponse = true;

    }
    }
    public string ReturnAnswer()
    {
        return curlDW.outText;
    }
    
}
