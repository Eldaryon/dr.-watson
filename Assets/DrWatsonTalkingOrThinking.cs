﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrWatsonTalkingOrThinking : MonoBehaviour
{
    Image associatedImage;
    Color baseColor;
    // Start is called before the first frame update
    void Start()
    {
        associatedImage = GetComponent<Image>();
        baseColor = associatedImage.color;
    }

    // Update is called once per frame
    void Update()
    {
        if(LoginWrapper.wrapper.listening)
            associatedImage.color = Color.green;
        else
            associatedImage.color = baseColor;
    }
}
