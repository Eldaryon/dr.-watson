﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextBox_WriteAsItGoes : MonoBehaviour
{
    TextMeshProUGUI associatedText;
    bool LaunchedSynth = false;
    // Start is called before the first frame update
    void Start()
    {
      associatedText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if(associatedText.text != DialogueManager.instance.line && !DialogueManager.instance.finishedSpeaking)
        {
            // Certified Functional.
            if(!LaunchedSynth && LoginWrapper.wrapper.ttsStarted)
            {
                LaunchedSynth = true;
                LoginWrapper.wrapper.SynthesizeSentence(DialogueManager.instance.line);
            }
            //This works too, as demonstrated by the text advance
            if(!DialogueManager.instance.speaking && LoginWrapper.wrapper.PermitTextAdvanceSynth() 
            && LoginWrapper.wrapper.tAStarted)
            {
                StartCoroutine(SpeakLetterByLetter());
            }
        }
        else
        {
            if(!string.IsNullOrEmpty(LoginWrapper.wrapper.line))
            {
                if (DialogueManager.instance.line != LoginWrapper.wrapper.line)
                    {
                        DialogueManager.instance.line = LoginWrapper.wrapper.line;
                    }
            }
            if(LoginWrapper.wrapper.assDelivered)
            {
                    associatedText.text = "";
                    LoginWrapper.wrapper.assDelivered = false;
                    LoginWrapper.wrapper.finishedListening = false;
                    DialogueManager.instance.finishedSpeaking = false;
                    
                if(LoginWrapper.wrapper.goodbye)
                {
                    StartCoroutine(QuitApp());
                }                
            }
            if(!LoginWrapper.wrapper.listening && !LoginWrapper.wrapper.finishedListening && DialogueManager.instance.finishedSpeaking)
            {
                if(!LoginWrapper.wrapper.obtainedModelResponse)
                {
                Debug.Log("Starting the listener.");
                LoginWrapper.wrapper.ListentoUser();
                }
                else
                {
                if(LoginWrapper.wrapper.modelResponse.Contains("1"))
                        DialogueManager.instance.line = "I'm sorry to announce that your patient has high chances of suffering from a heart disease.";
                else
                        DialogueManager.instance.line = "I've got great news! Your patient has not that much chances of having a heart disease!";
                LoginWrapper.wrapper.obtainedModelResponse = false;
                LoginWrapper.wrapper.modelResponse = "";
                LoginWrapper.wrapper.model = null;
                LoginWrapper.wrapper.finishedListening = true;
                LoginWrapper.wrapper.assDelivered = true;
                }
                    
            }
        }
    }
    
    IEnumerator SpeakLetterByLetter()
    {
        DialogueManager.instance.speaking = true;
        AudioSource tempAudio = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        if (tempAudio != null)
            Debug.Log("Audio Loaded: " + tempAudio.ToString());
        tempAudio.Play();
        string tempText;
        tempText = DialogueManager.instance.line;
        foreach (char character in tempText)
        {
            associatedText.text += character;
            yield return new WaitForSeconds(DialogueManager.instance.speakSpeed);
        }
        DialogueManager.instance.speaking = false;
        DialogueManager.instance.finishedSpeaking = true;
        LaunchedSynth = false;
    }
    IEnumerator QuitApp()
    {
        //Debug.Log("Quiting");
        yield return new WaitForSeconds(1f);
        Application.Quit();
    }
}
